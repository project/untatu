<?php

namespace Drupal\untatu\Plugin\views\filter;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\views\Plugin\views\filter\BooleanOperator;
use Drupal\views\Plugin\ViewsHandlerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * This filter checks the default langcode translation as well.
 *
 * The default usage is: when the default translation gets unpublished, all
 * translations became access denied to the public. But the published field is
 * still translatable, it's possible to have the default translation published
 * but some translations unpublished.
 *
 * Most of the code is copied from LatestTranslationAffectedRevision.
 *
 * @ViewsFilter("untatu")
 */
class Untatu extends BooleanOperator implements ContainerFactoryPluginInterface {

  /**
   * Entity Type Manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Views Handler Plugin Manager.
   *
   * @var \Drupal\views\Plugin\ViewsHandlerManager
   */
  protected $joinHandler;

  /**
   * Constructs a new LatestRevision.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity Type Manager Service.
   * @param \Drupal\views\Plugin\ViewsHandlerManager $join_handler
   *   Views Handler Plugin Manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ViewsHandlerManager $join_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->joinHandler = $join_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration, $plugin_id, $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.views.join')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function queryOpBoolean($field, $query_operator = self::EQUAL) {
    parent::queryOpBoolean($field, $query_operator);
    if ($query_operator === self::EQUAL && !empty($this->value)) {
      /** @var \Drupal\views\Plugin\views\query\Sql $query */
      $query = $this->query;
      $query_base_table = $this->relationship ?: $this->view->storage->get('base_table');

      $entity_type = $this->entityTypeManager->getDefinition($this->getEntityType());
      $keys = $entity_type->getKeys();

      $definition = [
        'table' => $query_base_table,
        'type' => 'INNER',
        'field' => $keys['revision'],
        'left_table' => $query_base_table,
        'left_field' => $keys['revision'],
        'extra' => [
          [
            'field' => $this->realField,
            'value' => 1,
            'operator' => '=',
          ],
          [
            'field' => 'default_langcode',
            'value' => 1,
            'operator' => '=',
          ],
        ],
      ];
      /** @var \Drupal\views\Plugin\views\join\JoinPluginBase $join */
      $join = $this->joinHandler->createInstance('standard', $definition);
      $query->addTable($query_base_table, $this->relationship, $join);
    }
  }

}
