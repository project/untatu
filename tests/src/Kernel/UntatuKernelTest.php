<?php

namespace Drupal\Tests\untatu\Kernel;

use Drupal\entity_test\Entity\EntityTestMulRevPub;
use Drupal\KernelTests\KernelTestBase;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\language\LanguageNegotiatorInterface;
use Drupal\language\Plugin\LanguageNegotiation\LanguageNegotiationUrl;
use Drupal\views\Views;
use Prophecy\Argument;

/**
 * Tests untatu functionality.
 *
 * @group untatu
 */
class UntatuKernelTest extends KernelTestBase {

  public static $modules = [
    'entity_test',
    'language',
    // 'untatu' will be enabled as needed.
    'untatu_test',
    'system',
    'user',
    'views',
  ];

  /**
   * The language Negotiator.
   *
   * @var \Drupal\language\LanguageNegotiatorInterface
   */
  protected $negotiator;

  /**
   * {@inheritdoc}
   */
  public function setup() {
    parent::setUp();

    $this->installConfig(['untatu_test']);
    $this->installEntitySchema('entity_test_mulrevpub');

    // Prepare a negotiator which always returns the German language.
    $language = ConfigurableLanguage::create(['id' => 'de']);
    $language->save();
    $negotiator = $this->prophesize(LanguageNegotiatorInterface::class);
    $negotiator->initializeType(Argument::any())->willReturn([
      LanguageNegotiationUrl::METHOD_ID => $language,
    ]);
    $this->negotiator = $negotiator->reveal();
  }

  /**
   * Tests the untatu views filter.
   *
   * @dataProvider providerViewsFilter
   */
  public function testUntatuViewsFilter($englishStatus, $germanStatus, $untatuStatus) {
    $entity = EntityTestMulRevPub::create(['type' => 'default', 'name' => $this->randomString()]);
    $englishStatus ? $entity->setPublished() : $entity->setUnpublished();;
    $entity->addTranslation('de', ['name' => $this->randomString()]);
    $translation = $entity->getTranslation('de');
    if ($germanStatus) {
      $translation->setPublished();
      // If it is published but the english translation is not and untatu is
      // on then no rows are expected. This is the primary use case of the
      // module.
      if (!$englishStatus && $untatuStatus) {
        $expected = 0;
      }
      // Otherwise one row should be returned.
      else {
        $expected = '1';
      }
    }
    else {
      $translation->setUnpublished();
      // If the german translation is unpublished, then there can no rows.
      $expected = 0;
    }
    // Make sure the status field is set correctly. It does not strictly belong
    // to this test but since we co-opted an entity from entity_test, it's
    // better to make sure.
    $this->assertSame($englishStatus, $entity->getUntranslated()->isPublished());
    $this->assertSame($germanStatus, $entity->getTranslation('de')->isPublished());
    $entity->save();
    if ($untatuStatus) {
      $this->enableModules(['untatu']);
    }
    /** @var \Drupal\language\ConfigurableLanguageManager $lm */
    $lm = $this->container->get('language_manager');
    // Needs to be done here because enabling the untatu module rebuilds the
    // container.
    $lm->setNegotiator($this->negotiator);
    // This test filters om status = 1 and
    // langcode = '***LANGUAGE_language_interface***'.
    $view = Views::getView('untatu_test');
    $view->get_total_rows = TRUE;
    $view->execute();

    $this->assertSame($expected, $view->total_rows);
  }

  /**
   * {@inheritdoc}
   */
  public function providerViewsFilter() {
    // Generate all possible three long TRUE-FALSE combinations.
    for ($i = 0; $i < 8; $i++) {
      $return[] = [!!($i & 4), !!($i & 2), !!($i & 1)];
    }
    return $return;
  }

}
